package Operations;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import Payloads.Userdataforputreq;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class PUTRequest {
	
	
	
	
	@Test
	public void updateUserInfo()
	{
		Userdataforputreq Userdataforputreq=new Userdataforputreq();
		
		Userdataforputreq.setLast_name("mahesh");
		Userdataforputreq.setFirst_name("kumar");
		Userdataforputreq.setId("2");
		
	    Response resp=given().
			body(Userdataforputreq).
			when().
	        contentType(ContentType.JSON).
			put("https://reqres.in/api/users/2").then().assertThat().
            statusCode(200).contentType(ContentType.JSON).
            and().
            log().
            body().
            extract().response();

}
}