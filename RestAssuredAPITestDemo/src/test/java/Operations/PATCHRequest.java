package Operations;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import Payloads.UserdataforPatch;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class PATCHRequest {
	
	@Test
	public void updateUserDataPartially()
	{
		UserdataforPatch userdataforPatch= new UserdataforPatch();
		
		userdataforPatch.setJob("software");
		userdataforPatch.setName("Mahesh");
		
	    Response resp=given().
			body(userdataforPatch).
			when().
	        contentType(ContentType.JSON).
			put("https://reqres.in/api/users/2").then().assertThat().
            statusCode(200).contentType(ContentType.JSON).
            and().
            log().
            body().
            extract().response();
	}

}
