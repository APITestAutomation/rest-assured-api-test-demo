package Operations;

import static io.restassured.RestAssured.given;
import org.testng.Reporter;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;


public class GETRequest {
	
	

	@Test
	public void weatherData()
	{
		
		RestAssured.baseURI="http://api.openweathermap.org/data/2.5/weather/";
		
		Response res= given().param("q","London")
				             .param("APPID", "f9b8dada7ffc9367d1d1bdd275a6a74b")
				              .get().
				              then().
				              assertThat().
				              statusCode(200).contentType(ContentType.JSON).
				              and().
				              log().
				              body().
				              extract().response();
			
		Reporter.log("response is:"+res.asString());
		
		Headers allHeaders = res.headers();
		 
		// Iterate over all the Headers
		for(Header header : allHeaders)
		{
			System.out.println("Key: " + header.getName() + " Value: " + header.getValue());
		}
		
	

		
		//ExtentTestManager.getTest().log(LogStatus.INFO, "Response is: " +res.asString());
		
		
	}

}
