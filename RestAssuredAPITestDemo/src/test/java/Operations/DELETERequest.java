package Operations;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class DELETERequest {
	
	@Test
	public void deleteUserData()
	{
		Response resp=given().
	
				when().
		        contentType(ContentType.JSON).
				delete("https://reqres.in/api/users/2").then().assertThat().
	            statusCode(204).
	            and().
	            log().
	            body().
	            extract().response();
	}

}
