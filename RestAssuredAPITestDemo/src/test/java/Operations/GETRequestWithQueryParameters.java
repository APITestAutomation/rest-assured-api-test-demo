package Operations;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class GETRequestWithQueryParameters {
	
	
		
		@Test
	public void queryParameterGetReqTest()
	{
	RestAssured.baseURI="https://restcountries.eu/rest/v2/name/aruba";
	
	Response res= given().queryParam("fullText","true")
			             .get().
			              then().
			              assertThat().
			              statusCode(200).contentType(ContentType.JSON).
			              and().
			              log().
			              body().
			              extract().response();
		
	Reporter.log("response is:"+res.asString());
	
	String contentType = res.header("Content-Type");
	System.out.println("Content-Type value: " + contentType);
	Assert.assertEquals(contentType, "application/json;charset=utf-8");
	
	String serverType =  res.header("Server");
	System.out.println("Server value: " + serverType);
	
	String acceptLanguage = res.header("Content-Encoding");
	System.out.println("Content-Encoding: " + acceptLanguage);
	
	
		}

}
