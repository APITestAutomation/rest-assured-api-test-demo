package Operations;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.Test;


import com.relevantcodes.extentreports.LogStatus;

import Payloads.Userdata;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class POSTRequest {
	
	
	
	@Test
	public void registerUser()
	{
		
		
			
			Userdata obj=new Userdata();
			obj.setEmail("rakeshkumar123@gmail.com");
			obj.setPassword("maheshr123");
			
			Response resp=given().
			body(obj).
			when().
	        contentType(ContentType.JSON).
			post("https://reqres.in/api/register").then().assertThat().
            statusCode(201).contentType(ContentType.JSON).
            and().
            log().
            body().
            extract().response();
			
		
	}

}
