package GitJsonServerExamples;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class POSTTest {
	
	@Test
	public void PostOperation()
	{
		Response resp=given().
				body(" {\"id\": \"17\", \"title\": \"TeamLead123\", \"author\": \"Anandkumar\"}").
				when().
		        contentType(ContentType.JSON).
				post("http://localhost:3000/posts").then().assertThat().
	            statusCode(201).contentType(ContentType.JSON).
	            and().
	            log().
	            body().
	            extract().response();
				
			
	}

}
