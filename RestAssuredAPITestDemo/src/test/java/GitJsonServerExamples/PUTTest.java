package GitJsonServerExamples;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class PUTTest {
	
	@Test
	public void PutOperation()
	{
		Response resp=given().
				body(" {\"id\": \"5\", \"title\": \"AdminSoftware\", \"author\": \"AnandR\"}").
				when().
		        contentType(ContentType.JSON).
				put("http://localhost:3000/posts/5").then().assertThat().
	            contentType(ContentType.JSON).
	            and().
	            log().
	            body().
	            extract().response();
				
			
	}

}
