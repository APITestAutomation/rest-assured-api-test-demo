package GitJsonServerExamples;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class PatchTest {
	
	@Test
	public void patchOperation()
	{
		Response res= given().body("{\"id\": \"1\", \"title\": \"Rest Assured API Automation\", \"author\": \"Siva kumar\"}").
				    when().
				    contentType(ContentType.JSON).
				    patch("http://localhost:3000/posts/1").
				    then().
				    statusCode(200)
				    .log().all().
				    extract().
				    response();
		
	}

}
