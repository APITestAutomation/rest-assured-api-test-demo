# Rest Assured API Test Demo

#Basic Rest API for put post delete patch 
#https://reqres.in/

#json server API to do real operations for PUT POST DELETE PATCH
#if we want to start json server first we need to install nodejs in machine

#https://github.com/typicode/json-server

#npm install -g json-server
#json-server --watch db.json

# http://localhost:3000/posts/1
#{ "id": 1, "title": "json-server", "author": "typicode" }